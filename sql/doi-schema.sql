--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Name: doc_id; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE doc_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.doc_id OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: tbl_doc; Type: TABLE; Schema: public; Owner: doi; Tablespace:
--

CREATE TABLE tbl_doc (
    doc_url character varying(256) NOT NULL,
    doc_id integer DEFAULT nextval('doc_id'::regclass) NOT NULL,
    doc_xml text NOT NULL,
    doc_status character varying(1024),
    doc_doi character varying(128),
    doc_date timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone NOT NULL,
    doc_title character varying(512),
    user_id character varying(256),
    doc_active boolean DEFAULT false
);


ALTER TABLE public.tbl_doc OWNER TO doi;

--
-- Name: tbl_url; Type: TABLE; Schema: public; Owner: doi; Tablespace:
--

CREATE TABLE tbl_url (
    url character varying(1024) NOT NULL,
    facilities character varying(1024) NOT NULL,
    approved character varying(10),
    email character varying(128)
);


ALTER TABLE public.tbl_url OWNER TO doi;

--
-- Name: tbl_user; Type: TABLE; Schema: public; Owner: doi; Tablespace:
--

CREATE TABLE tbl_user (
    username character varying(128) NOT NULL,
    facility character varying(1024),
    email character varying(128) NOT NULL,
    approved boolean,
    user_id character varying(128),
    enabled boolean,
    data_manager boolean DEFAULT false,
    appid_seed character varying(4)
);


ALTER TABLE public.tbl_user OWNER TO doi;

--
-- Name: email; Type: CONSTRAINT; Schema: public; Owner: doi; Tablespace:
--

ALTER TABLE ONLY tbl_user
    ADD CONSTRAINT email PRIMARY KEY (email);


--
-- Name: pk_doc_id; Type: CONSTRAINT; Schema: public; Owner: doi; Tablespace:
--

ALTER TABLE ONLY tbl_doc
    ADD CONSTRAINT pk_doc_id PRIMARY KEY (doc_id);


--
-- Name: tbl_url_pkey; Type: CONSTRAINT; Schema: public; Owner: doi; Tablespace:
--

ALTER TABLE ONLY tbl_url
    ADD CONSTRAINT tbl_url_pkey PRIMARY KEY (url);


--
-- Name: doc_doi; Type: INDEX; Schema: public; Owner: doi; Tablespace:
--

CREATE INDEX doc_doi ON tbl_doc USING btree (doc_doi);


--
-- Name: doc_title; Type: INDEX; Schema: public; Owner: doi; Tablespace:
--

CREATE INDEX doc_title ON tbl_doc USING btree (doc_title);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: doc_id; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE doc_id FROM PUBLIC;
REVOKE ALL ON SEQUENCE doc_id FROM postgres;
GRANT ALL ON SEQUENCE doc_id TO postgres;
GRANT ALL ON SEQUENCE doc_id TO doi;


--
-- Name: tbl_doc; Type: ACL; Schema: public; Owner: doi
--

REVOKE ALL ON TABLE tbl_doc FROM PUBLIC;
REVOKE ALL ON TABLE tbl_doc FROM doi;
GRANT ALL ON TABLE tbl_doc TO doi;


--
-- PostgreSQL database dump complete
--
